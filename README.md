[![pipeline status](https://gitlab.com/alfabc/terms-smart-contract/badges/master/pipeline.svg)](https://gitlab.com/alfabc/terms-smart-contract/commits/master)
[![coverage report](https://gitlab.com/alfabc/terms-smart-contract/badges/master/coverage.svg)](https://gitlab.com/alfabc/terms-smart-contract/commits/master)

# Terms fields for smart-contracts

## Overview

Smart-contract Solidity library which provides two fields to store both terms & conditions location and hash :
- **location** : a string containing a link to the document
- **hash** : 32-bytes hash associated to this document. Usually KECCAK256 (SHA-3)

## Usage

- Import library from npm :
```
npm install @alfabc/contract-terms
```

- Import library in your code to use it :
```solidity
pragma solidity ^0.4.25;

import "@alfabc/contract-terms/contracts/ContractTerms.sol";

contract NewContract is ContractTerms {
  constructor() ContractTerms(
    "/url/location/of/document",
    "0x9c22ff5f21f0b81b113e63f7db6da94fedef11b2119b4088b89664fb9a3cb658"
  ) public {
    // Put your code here
  }
}
```

## Interface

This library exposes two external getters... and that's all :
- `getLocation()` : returns the location of the terms document
- `getHash()` : returns the hash of the terms document