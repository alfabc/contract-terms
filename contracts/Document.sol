pragma solidity ^0.4.25;

interface Document {
    function getLocation() external view returns (string memory);
    function getHash() external view returns (bytes32);
}