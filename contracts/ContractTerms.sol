pragma solidity ^0.4.25;

import "./Document.sol";

contract ContractTerms is Document {
    string public termsLocation;
    bytes32 termsHash;

    constructor(string memory _termsLocation, bytes32 _termHash) public {
        termsLocation = _termsLocation;
        termsHash = _termHash;
    }

    function getLocation() external view returns (string memory) {
        return termsLocation;
    }

    function getHash() external view returns (bytes32) {
        return termsHash;
    }
}