const ContractTerms = artifacts.require('../contracts/ContractTerms')

const should = require('chai')
  .use(require('chai-as-promised'))
  .should()

contract('ContractTerms', () => {
  let terms;
  context('initialization', () => {
    it('Should accept correct constructor arguments', async () => {
      terms = await ContractTerms.new(
        'url/location/of/contract',
        '0x9c22ff5f21f0b81b113e63f7db6da94fedef11b2119b4088b89664fb9a3cb658'
      );
      (await terms.getLocation()).should.eq('url/location/of/contract');
      (await terms.getHash()).should.eq('0x9c22ff5f21f0b81b113e63f7db6da94fedef11b2119b4088b89664fb9a3cb658')
    })
  })
})